package net.dwade.livechat.websocket.beans;

public enum SubscribeType {
	
	CHAT_MESSAGE( "/user/queue/chat/newMsg" ),
	
	CHAT_RESPONSE( "/user/queue/chat/msgResponse" ),
	
	SYSTEM_MESSAGE( "/user/queue/system/newMsg" ),
	
	SYSTEM_CLOSE( "/user/queue/system/close" );
	
	/**
	 * 订阅地址
	 */
	public final String destination;
	
	private SubscribeType( String destination ) {
		this.destination = destination;
	}
	
	public String destination() {
		return this.destination;
	}
	
}
