package net.dwade.livechat.websocket.beans;

public class WebSocketMessageResponse {
	
	/**
	 * 用于标识页面上的消息
	 */
	private String websocketRequestId;
	
	private String retCode = "000000";

	private String retMsg = "success";

	public String getWebsocketRequestId() {
		return websocketRequestId;
	}

	public void setWebsocketRequestId(String websocketRequestId) {
		this.websocketRequestId = websocketRequestId;
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

}
