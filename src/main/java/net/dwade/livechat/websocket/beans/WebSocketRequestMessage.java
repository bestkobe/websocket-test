package net.dwade.livechat.websocket.beans;

import java.util.Date;

public class WebSocketRequestMessage {

	/**
	 * 如果是notice的话，固定写system
	 */
	private String userId;
	
	private String sessionId;
	
	private String message;
	
	/**
	 * 0 访客消息  1 客服消息 2 系统消息 3 其他
	 */
	private String messageType;
	
	private Date postTime;
	
	/**
	 * 0 文本 1 图片 2 音频 3 表情符号 4 超链接
	 */
	private String contentType;
	
	private String toUserId;
	
	private String messageId;
	
	/**
	 * 用于标识页面上的消息
	 */
	private String websocketRequestId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Date getPostTime() {
		return postTime;
	}

	public void setPostTime(Date postTime) {
		this.postTime = postTime;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getToUserId() {
		return toUserId;
	}

	public void setToUserId(String toUserId) {
		this.toUserId = toUserId;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getWebsocketRequestId() {
		return websocketRequestId;
	}

	public void setWebsocketRequestId(String websocketRequestId) {
		this.websocketRequestId = websocketRequestId;
	}
	
}
