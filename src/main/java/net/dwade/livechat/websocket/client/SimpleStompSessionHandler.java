package net.dwade.livechat.websocket.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

/**
 * 测试Websocket连接的StompSessionHandler
 * @author huangxf
 * @date 2016年12月19日
 */
public class SimpleStompSessionHandler extends StompSessionHandlerAdapter {
	
	private static final Logger logger = LoggerFactory.getLogger( SimpleStompSessionHandler.class );

	@Override
	public void afterConnected(StompSession session,
			StompHeaders connectedHeaders) {
		logger.debug( "Stomp session创建成功:{}", session.getSessionId() );
	}

	@Override
	public void handleTransportError(StompSession session, Throwable exception) {
		logger.error( "Websocket异常", exception );
	}

}
