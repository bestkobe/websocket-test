package net.dwade.livechat.websocket.client;

import org.springframework.messaging.simp.stomp.StompSession;

/**
 * 不需要订阅消息的Webosocket客户端
 * @author huangxf
 * @date 2016年12月30日
 */
public final class NoSubscritionChatWebsocketClient extends ChatWebsocketClient {

	public NoSubscritionChatWebsocketClient(String userAgent, String indexUrl,
			String pullUrl, String chatUrl, String domain, String httpSessionId) {
		super(userAgent, indexUrl, pullUrl, chatUrl, domain, httpSessionId);
	}
	
	@Override
	protected void subscribe( StompSession session ) {
		// don't do subscribe operation.
	}

}
