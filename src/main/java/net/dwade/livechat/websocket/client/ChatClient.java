package net.dwade.livechat.websocket.client;

import net.dwade.livechat.websocket.beans.WebSocketRequestMessage;

/**
 * 访客客户端
 * @author huangxf
 * @date 2016年12月30日
 */
public interface ChatClient {
	
	/**
	 * 模拟用户进入聊天首页
	 */
	public void index();
	
	/**
	 * 轮询排队结果
	 */
	public void clientPull();
	
	/**
	 * 创建websocket连接
	 */
	public void connect();
	
	/**
	 * 关闭websocket连接
	 */
	public void disconnect();
	
	/**
	 * 发送websocket消息
	 * @param text
	 * @return
	 */
	public WebSocketRequestMessage sendMessage( String text );
	
	/**
	 * http请求头，User-Agent
	 * @return
	 */
	public String getUserAgent();

	public String getIndexUrl();

	public String getPullUrl();

	public String getChatUrl();

	public String getDomain();
	
	public String getChatSessionId();

}
