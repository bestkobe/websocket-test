package net.dwade.livechat.websocket.client;

/**
 * 用于websocket client是否正常
 * @author huangxf
 * @date 2018年1月14日
 */
public class WebsocketClientMain {
	
	public static void main(String[] args) {
		String USER_AGENT = "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.76 Mobile Safari/537.36";
		String URL_INDEX = "http://localhost/livechat-touch-client/index?tenantId=150298&code=BS&channelType=1021";
		String URL_PULL = "http://localhost/livechat-touch-client/clientPull";
		String URL_CHAT = "ws://localhost/livechat-touch-client/chat";
		String DOMAIN = "localhost";
		String HTTP_SESSION_ID = null;
		ChatClient client = new TimeLoggingChatWebsocketClient( USER_AGENT, 
				URL_INDEX, URL_PULL, URL_CHAT, DOMAIN, HTTP_SESSION_ID );
		client.index();
		client.clientPull();
		client.connect();
		for ( int i = 0; i < 5; i++ ) {
			System.out.println( "Start:" + System.currentTimeMillis() );
			client.sendMessage( "Hello world." );
		}
		
		try {
			Thread.sleep( 10 * 60 * 1000 );
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
