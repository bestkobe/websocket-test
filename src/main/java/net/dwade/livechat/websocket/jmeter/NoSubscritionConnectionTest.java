package net.dwade.livechat.websocket.jmeter;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.dwade.livechat.websocket.client.ChatClient;
import net.dwade.livechat.websocket.client.NoSubscritionChatWebsocketClient;

/**
 * 建立连接的jmeter测试，该场景循环创建Websocket连接，不会保持已创建的会话，并且不会发出订阅操作
 */
public class NoSubscritionConnectionTest extends AbstractJavaSamplerClient {
	
	private static Logger logger = LoggerFactory.getLogger( NoSubscritionConnectionTest.class );

	private static String label = "WebsocketConnectionTest";
	
	private ChatClient client;
	
	/**
	 * 执行runTest()方法前会调用此方法,可放一些初始化代码
	 */
	@Override
	public void setupTest(JavaSamplerContext context) {
		
		//初始化参数
		String userAgent = context.getParameter( "USER_AGENT" );
		String indexUrl = context.getParameter( "URL_INDEX" );
		String pullUrl = context.getParameter( "URL_PULL" );
		String chatUrl = context.getParameter( "URL_CHAT" );
		String domain = context.getParameter( "DOMAIN" );
		String httpSessionId = context.getParameter( "HTTP_SESSION_ID" );
		client = new NoSubscritionChatWebsocketClient( userAgent, indexUrl, pullUrl, chatUrl, domain, httpSessionId );
		
		logger.info( "Creat websocket client: {}", client );
		client.index();
		client.clientPull();
		logger.info( "Websocket连接已就绪, chatSessionId:{}", client.getChatSessionId() );
	}
	
	/**
	 * JMeter测试用例入口
	 */
	@Override
	public SampleResult runTest( JavaSamplerContext context ) {
		//再次创建连接之前，先关闭之前的连接
		try {
			client.disconnect();
		} catch (Exception e) {
			// ignore exception
		}
		SampleResult sr = new SampleResult();
		sr.setSampleLabel( label );
		sr.sampleStart();
		try {
			client.connect();
			sr.setSamplerData( "Success" );
			sr.setSuccessful( true );
		} catch (Throwable e) {
			logger.error( "Websocket连接失败！", e );
			sr.setSamplerData( e.getMessage() );
			sr.setSuccessful( false );
		} finally {
			sr.sampleEnd();
		}
		return sr;
	}

	/**
	 * 指定JMeter界面中可手工输入的参数
	 */
	@Override
	public Arguments getDefaultParameters() {
		Arguments args = new Arguments();
		args.addArgument( "USER_AGENT", "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.76 Mobile Safari/537.36" );
		args.addArgument( "URL_INDEX", "http://localhost:10086/livechat-touch-client/index?tenantId=1000055&code=BS&channelType=1021" );
		args.addArgument( "URL_PULL", "http://localhost:10086/livechat-touch-client/clientPull" );
		args.addArgument( "URL_CHAT", "ws://localhost:10086/livechat-touch-client/chat" );
		args.addArgument( "DOMAIN", "10.255.201.166:18800" );
		args.addArgument( "HTTP_SESSION_ID", null );
		return args;
	}

	/**
	 * 线程测试结束后会调用此方法.
	 */
	@Override
	public void teardownTest( JavaSamplerContext context ) {
		client.disconnect();
		logger.debug( "After test." );
	}
	
}
