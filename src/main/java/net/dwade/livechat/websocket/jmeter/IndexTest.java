package net.dwade.livechat.websocket.jmeter;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.dwade.livechat.websocket.client.ChatClient;
import net.dwade.livechat.websocket.client.TimeLoggingChatWebsocketClient;

/**
 * 发送消息并发测试类，创建好Websocket连接后，并发发送消息，不需要等待服务端响应即认为一个事务结束 
 * @author huangxf
 * @date 2016年12月27日
 */
public class IndexTest extends AbstractJavaSamplerClient {
	
	private static Logger logger = LoggerFactory.getLogger( SubscritionConnectionTest.class );

	private static String label = "IndexTest";
	
	private ChatClient client;
	
	private String messageText;
	
	/**
	 * 执行runTest()方法前会调用此方法,可放一些初始化代码
	 */
	@Override
	public void setupTest(JavaSamplerContext context) {
		
		//初始化参数
		String userAgent = context.getParameter( "USER_AGENT" );
		String indexUrl = context.getParameter( "URL_INDEX" );
		String pullUrl = context.getParameter( "URL_PULL" );
		String chatUrl = context.getParameter( "URL_CHAT" );
		String domain = context.getParameter( "DOMAIN" );
		String httpSessionId = context.getParameter( "HTTP_SESSION_ID" );
		this.messageText = context.getParameter( "MESSAGE_TEXT" );
		
		//创建Websocket客户端
		client = new TimeLoggingChatWebsocketClient( userAgent, indexUrl, pullUrl, chatUrl, domain, httpSessionId );
		
		logger.info( "Creat client:{}", client.toString() );
		
	}
	
	/**
	 * JMeter测试用例入口
	 */
	@Override
	public SampleResult runTest( JavaSamplerContext context ) {
		SampleResult sr = new SampleResult();
		sr.setSampleLabel( label );
		sr.sampleStart();
		try {
			client.index();
			sr.setSamplerData( "Success" );
			sr.setSuccessful( true );
		} catch (Throwable e) {
			logger.error( "Websocket消息发送失败！", e );
			sr.setSamplerData( e.getMessage() );
			//用于设置运行结果的成功或失败，如果是"false"则表示结果失败，否则则表示成功
			sr.setSuccessful( false );
		} finally {
			sr.sampleEnd();
		}
		return sr;
	}

	/**
	 * 指定JMeter界面中可手工输入的参数
	 */
	@Override
	public Arguments getDefaultParameters() {
		Arguments args = new Arguments();
		args.addArgument( "USER_AGENT", "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.76 Mobile Safari/537.36" );
		args.addArgument( "URL_INDEX", "http://localhost:18800/livechat-touch-client/index?tenantId=1&code=BS&channelType=1021" );
		args.addArgument( "URL_PULL", "http://localhost:18800/livechat-touch-client/clientPull" );
		args.addArgument( "URL_CHAT", "ws://localhost:18800/livechat-touch-client/chat" );
		args.addArgument( "DOMAIN", "localhost:18800" );
		args.addArgument( "HTTP_SESSION_ID", null );
		args.addArgument( "MESSAGE_TEXT", "Hello world." );
		return args;
	}

	/**
	 * 线程测试结束后会调用此方法.
	 */
	@Override
	public void teardownTest( JavaSamplerContext context ) {
		//client.disconnect();
		logger.debug( "After test." );
	}
	
}
