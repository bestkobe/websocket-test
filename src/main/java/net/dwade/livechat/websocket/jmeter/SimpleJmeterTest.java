package net.dwade.livechat.websocket.jmeter;

import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;

public class SimpleJmeterTest extends AbstractJavaSamplerClient {

	@Override
	public SampleResult runTest(JavaSamplerContext context) {
		SampleResult sr = new SampleResult();
		sr.sampleStart();
		try {
			Thread.sleep( 2000 );
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println( "+++++++++Success++++++++" );
		sr.setSuccessful( true );
		sr.sampleEnd();
		return sr;
	}

	@Override
	public void setupTest(JavaSamplerContext context) {
		System.out.println( "setup test." );
	}

	@Override
	public void teardownTest(JavaSamplerContext context) {
		System.out.println( "#######After test.########" );
	}

}
