# websocket-test
压测的基本思想是通过java代码模拟浏览器websocket客户端与服务端通信，从而模拟真实的并发业务场景，再结合jmeter压测工具完成一系列的压测流程

java websocket客户端，基于spring websocket框架、stomp协议，使用SockJsClient客户端完成与服务端的消息交互

## 核心模块介绍
- net.dwade.livechat.websocket.client：核心代码，实现了websocket客户端，包括javax websocket的标准实现，以及xhr的实现
- net.dwade.livechat.websocket.jmeter：jmeter压测脚本
- net.dwade.livechat.websocket.loadrunner：lr的压测脚本，只完成一小部分

## 核心类介绍
- net.dwade.livechat.websocket.client.ChatClient：定义了websocket客户端，依据实际项目需求，先是使用http协议与客服创建对话，创建成功之后再使用websocket与客服对话
- net.dwade.livechat.websocket.client.ChatWebsocketClient：ChatClient的默认实现类，实现了标准的websocket客户端，以及http轮询的xhr客户端，默认使用标准的websocket客户端，并且它还会订阅消息推送
- net.dwade.livechat.websocket.client.NoSubscritionChatWebsocketClient：在创建websocket连接之后，不会订阅各种消息
- net.dwade.livechat.websocket.client.TimeLoggingChatWebsocketClient：继承至ChatWebsocketClient，支持记录消息发送及响应时间
- net.dwade.livechat.websocket.client.WebsocketClientMain：包括main方法，用于测试websococket客户端是否正常工作
- net.dwade.livechat.websocket.jmeter.SendMessageTest：jmeter测试代码，用于测试websocket发送消息的性能（主要是指服务端性能）